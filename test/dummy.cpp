#include "utils.h"
#include "buildConfigs.h"
#include <gtest/gtest.h>

class dummierSuite : public ::testing::Test {
public:
    int answer;

    std::string prefix = appPaths;
    std::vector<float> data;

    void SetUp() override
    {
        // Prepare some necessary data here
        answer = 42;

        data = loadData(prefix + "/data/kernels/unity.dat");
    }

    // void TearDown() override {}
};

TEST(dummySuite, dummy)
{
    // Expect two strings not to be equal.
    EXPECT_STRNE("hello", "world");
    // Expect equality.
    EXPECT_EQ(7 * 6, 42);
}

TEST_F(dummierSuite, dummier)
{
    // Expect two ints to be equal.
    EXPECT_EQ(42, answer);
}

TEST_F(dummierSuite, testLoadKernel)
{
    auto kernel = loadData("./../../data/kernels/unity.dat");

    ASSERT_EQ(kernel.size(), data.size());

    for (int i = 0; i < data.size(); ++i) {
        ASSERT_EQ(data[i], kernel[i]);
    }
}

TEST_F(dummierSuite, dummiest)
{
    auto data2 = loadData(prefix + "/data/kernels/test.dat");

    float sum = 0;
    for (auto& d : data2) {

        sum += d;
    }

    // Expect two floats to be nearly equal.
    EXPECT_NEAR(1, sum, 1e-3);
}

// TEST(dummySuite, newFunc)
// {
//     auto result = newFunc(14);

//     ASSERT_EQ(42, result);
// }
