#include "utils.h"

std::vector<float> loadData(const std::string& pth)
{

    std::ifstream f;
    f.open(pth);

    if (!f.is_open()) {
        std::cerr << "error: data file " << pth << " open failed\n";
        exit(1);
    }

    std::string line;

    std::vector<float> values;
    float val;
    float sum = 0;

    f >> val;

    while (f) {
        // std::cout << val << " ";
        values.push_back(val);
        sum += val;
        f >> val;
    }

    f.close();
    if (sum != 0)
        for (auto& v : values)
            v /= sum;

    return values;
}

// int newFunc(int inp)
// {
//     for (int i = 0; i < 9; i++)
//         inp += i;

//     return inp;
// }