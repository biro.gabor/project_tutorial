#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

std::vector<float> loadData(const std::string& pth);

// int newFunc(int inp);