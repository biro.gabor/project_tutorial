#include <iostream>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

void hello() { std::cout << "hello\n"; }

int main(int argc, char* argv[])
{
    hello();

    std::cout << "testing merge request pipeline\n";

    std::ifstream f;
    f.open(argv[1]);

    if (!f.is_open()) {
        std::cerr << "error: data file " << argv[1] << " open failed\n";
        exit(1);
    }

    std::string line;

    std::vector<float> data;
    float val;
    float sum = 0;

    f >> val;

    while (f) {
        // std::cout << val << " ";
        data.push_back(val);
        sum += val;
        f >> val;
    }

    f.close();
    if (sum != 0)
        for (auto& v : data)
            v /= sum;

    std::cout << "the loaded data:\n";
    for (auto& d : data)
        std::cout << d << " ";
    return 0;
}
