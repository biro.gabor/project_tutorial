#include <iostream>

#include "utils.h"
#include "mylib.h"

int main(int argc, char* argv[])
{
    hello();

    std::cout << "testing merge request pipeline\n";

    auto data = loadData(argv[1]);

    std::cout << "the loaded data:\n";
    for (auto& d : data)
        std::cout << d << " ";

    return 0;
}
