
CXX=g++

ODIR=build

IDIR=./include 
SDIR=./src 

LDIR=mylib
LIBS=-lmylib

CFLAGS=-I$(IDIR) -L$(LDIR) -I./mylib

_DEPS=utils.h
DEPS=$(patsubst %,$(IDIR)/%,$(_DEPS))

all: my_main

default: my_main

my_main: app/my_main.cpp $(ODIR)/utils.o
	$(CXX) $^ $(CFLAGS) $(LIBS)  -o $(ODIR)/$@ 

$(ODIR)/utils.o: src/utils.cpp include/utils.h
	$(CXX) -c -o $@ $< $(CFLAGS)

.PHONY: clean

clean:
	rm build/*.o build/my_main